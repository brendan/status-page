import 'document-register-element';
import isEmojiUnicodeSupported from './emoji/support';
import { emojiImageTag, emojiFallbackImageSrc } from './emoji';

class GlEmoji extends HTMLElement {
  constructor() {
    super();
    const emojiUnicode = this.textContent.trim();
    const { name, unicodeVersion, fallbackSrc } = this.dataset;

    const isEmojiUnicode =
      this.childNodes &&
      Array.prototype.every.call(this.childNodes, childNode => childNode.nodeType === 3);
    const hasImageFallback = fallbackSrc && fallbackSrc.length > 0;

    if (emojiUnicode && isEmojiUnicode && !isEmojiUnicodeSupported(emojiUnicode, unicodeVersion)) {
      if (hasImageFallback) {
        this.innerHTML = emojiImageTag(name, fallbackSrc);
      } else {
        const src = emojiFallbackImageSrc(name);
        this.innerHTML = emojiImageTag(name, src);
      }
    }
  }
}

export default function installGlEmojiElement() {
  if (!customElements.get('gl-emoji')) {
    customElements.define('gl-emoji', GlEmoji);
  }
}
